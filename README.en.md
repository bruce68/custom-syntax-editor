# custom-syntax-editor
Custom syntax editor

## Usage

Install

```sh
npm install custom-syntax-editor
```

Use

```jsx
import CustomEditor from "custom-syntax-editor";

const defaultCode = "";
const getCode = (code) => {
    console.log(code);
}

<CustomEditor
    defaultCode={defaultCode}
    readOnly={false}
    height={400}
    theme="night"
    activeLine={true}
    fold={true}
    keywords={keywords}
    onChange={getCode}
/>
```

## keywords
```jsx
const keywords = [
    // groovy keywords
    "as", "catch", "def", "enum", "for", "import", "new", "super", "throws", "while",
    "assert", "class", "default", "extends", "goto", "in", "null", "switch", "trait", "break",
    "const", "do", "false", "if", "instanceof", "package", "this", "true", "case", "continue", "else",
    "finally", "implements", "interface", "return", "throw", "try",
    // java keywords
    "abstract", "transient", "int", "strictfp", "synchronized", "boolean", "char", "do",
    "final", "private", "short", "void", "double", "long", "protected", "static", "volatile",
    "byte", "float", "native", "public",
    // JDK Commonly used class
    "System", "Runtime", "String", "StringBuffer", "StringBuilder", "Date", "DateFormat",
    "SimpleDateFormat", "Calendar", "GregorianGalendar", "Math", "Integer", "Double", "Float",
    "Boolean", "List", "HashMap", "Map", "ArrayList", "Arrays", "Random", "Iterator"
];
```

## Effect of editor
![Image text](https://gitee.com/bruce68/custom-syntax-editor/raw/master/src/img/pic.png)

## props：
|    parameter    | type    |  default value   |  If required  | instructions         |
| :------:  | :-----: | :----:   | :------: | :----------: |
| defaultCode | string |  ""     |   Not required    | Initializes the assignment     |
| readOnly  | boolean |  false   |   Not required  | Set the read-only       |
| height | number   |  300     |   Not required  | The editor height       |
| theme  | string   |  "day"     |   Not required  | Theme："day"和"night" |
| activeLine   | boolean   |  true     |   Not required  | The current line selected  |
| indentUnit   | number   |  4     |   Not required  | The TAB by several Spaces indentation  |
| fold   | boolean   |  true     |   Not required  | Code folding  |
| keywords   | array   |  []     |   Not required  | Custom hint keywords  |
| onChange  | function|   -      |   Not required  | return code       |
| Ctrl+F   | Keyboard events   |  -     |   Not required  | Automatic formatting code  |

## License
MIT